# Solution overview

This Reference Configuration for Red Hat OpenShift Container Platform (OCP) on HPE SimpliVity is part of an overall
solution from Hewlett Packard Enterprise. The solution consists of the hardware and software, along with the HPE services
options to help you deploy an enterprise-ready Kubernetes platform quickly and efficiently. The solution takes HPE
SimpliVity infrastructure and combines it with Red Hat's enterprise-grade OpenShift Container Platform and popular open
source tools, and deployment and advisory services that are available from HPE Pointnext.

`Kubernetes` is an orchestration system for managing container-based applications. Kubernetes empowers developers to
utilize new architectures like microservices and serverless that require developers to think about application operations
in a way they may not have before. These software architectures can blur the lines between traditional development and
application operations. 

`Red Hat OpenShift Container Platform` expands enterprise Kubernetes with full-stack automated operations to manage
hybrid cloud and multi-cloud deployments.

`HPE SimpliVity` is an enterprise-grade hyper-converged platform uniting best-in-class data services with the world's best-selling server.

