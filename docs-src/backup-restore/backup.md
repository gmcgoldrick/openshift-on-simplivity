# Backup


## Configuration

The following variables are used to configure your backup:


|Variable|File|Description|
|:-------|:---|:----------|
|`backup_directory`|group_vars/all/vars.yml|The directory on the Ansible controller node where all the backed up files are stored. The directory will be created if it does not exist.|
|`backup_artifacts`|group_vars/all/vars.yml|A list of files or directories on the Ansible controller node you wish to include in the etcd snapshots.  File paths are relative to the location of the playbook.|

In the example below, the contents of the `install_dir` directory are backed up, as well as the contents of the `group_vars` folder and the Ansible inventory file `hosts`.

```
backup_directory: "{{ local_home }}/backups"
backup_artifacts:
- "{{ install_dir }}"
- ./group_vars/
- ./hosts
```

To perform the backup, run the playbook `backup_etcd.yml`:

```
$ cd ~/OpenShift-on-SimpliVity
$ ansible-playbook -i hosts backup_etcd.yml
```

Based on the configuration above, the backed up files will be available in the `~/backups/` folder:

```bash
$ ls ~/backups/

backup_2019_09_19_155338.misc.tgz  
backup_2019_09_19_155338.snapshots.tgz
```

The `.misc.tgz` file contains backups of your hosts and variables files, as well as the files
generated by the initial deployment in the installation directory `install_dir`. If you need to
re-deploy your cluster from scratch, it is very convenient to have a backup of these files.

The `.snapshots.tgz` file contains the etcd database snapshots and certs that will be needed
in the following section [Recovering from lost master hosts](recovery-lost-master).




