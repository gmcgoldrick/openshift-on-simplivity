# Playbooks for cluster logging


After making the customizations appropriate to your environment, deploy the EFK stack by changing to the directory where you cloned the OpenShift-on-SimpliVity repository and running the `playbooks/efk.yml` playbook:

```bash
$ cd ~/OpenShift-on-SimpliVity

$ ansible-playbook -i hosts playbooks/efk.yml
```

The playbook takes approximately 1-2 minutes to complete.  However, it may take several additional minutes for the 
various Cluster Logging components to successfully deploy to the OpenShift Container Platform cluster.


