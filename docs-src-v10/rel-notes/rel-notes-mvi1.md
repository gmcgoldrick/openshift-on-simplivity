# About this release

This release has been tested using Red Hat Openshift Container Platform (OCP) 4.1.16, which is based on Kubernetes 1.13.

OpenShift Container Platform 4.1 supports Red Hat Enterprise Linux (RHEL) 7.6, as well as Red Hat 
Enterprise Linux CoreOS (RHCOS) 4.1. You must use CoreOS  for the master control plane machines and 
can use either CoreOS or RHEL 7.6 for worker compute machines. Two CoreOS worker nodes are required for the initial deployment of the cluster, but these can be replaced subsequently with RHEL nodes if required. Because  version 7.6 of Red Hat Enterprise
Linux is the only non-RHCOS version of Red Hat Enterprise Linux supported for compute machines, you must not upgrade these RHEL compute machines to version 8.

You must install the OpenShift Container Platform cluster on a VMware vSphere instance running version 6.5 or 6.7U2 or later instance. VMware recommends using vSphere Version 6.7 U2 or later with your OpenShift Container Platform cluster.

See the Red Hat OCP 4.1 release notes at [https://docs.openshift.com/container-platform/4.1/release_notes/ocp-4-1-release-notes.html](https://docs.openshift.com/container-platform/4.1/release_notes/ocp-4-1-release-notes.html)


OpenShift Container Platform 4.1 requires all machines, including the computer that you run the 
installation process on, to have direct internet access to pull images for platform containers 
and provide a limited amount of telemetry data to Red Hat. You cannot specify a proxy server for OpenShift Container Platform. By installing OpenShift Container Platform 4, you accept Red Hat's data collection policy. Learn more about the data collected at 
[https://docs.openshift.com/container-platform/4.1/telemetry/about-telemetry.html](https://docs.openshift.com/container-platform/4.1/telemetry/about-telemetry.html)