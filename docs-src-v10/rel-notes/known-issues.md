# Known issues

Known issues in this release of Red Hat OpenShift Container Platform (OCP) on HPE SimpliVity include:


- This release only supports OpenShift Container Platform version 4.1. In particular, it does not support the recent 4.2 release of the OCP software.

