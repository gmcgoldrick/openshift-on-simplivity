# Software

The software components used in this Reference Configuration are listed below.


**Table.** Third-party software

|Component|Version|
|:-------|:---|
|Ansible|2.8.1|
|Red Hat OpenShift Container Platform|4.1.*<br>Tested using 4.1.16| 
|Red Hat CoreOS|4.1.0|
|Red Hat Enterprise Linux| 7.6| 
|VMware|vCenter 6.7 U2|

<br>
<br>

**Table.** HPE Software


|Component|Version|
|:-------|:---|
|HPE SimpliVity OmniStack|3.7.10|


## About Ansible
Ansible is an open-source automation engine that automates software provisioning, configuration management and application deployment.

As with most configuration management software, Ansible has two types of servers: the controlling machine and the nodes. A single controlling machine orchestrates the nodes by deploying modules to the Linux nodes over SSH. The modules are temporarily stored on the nodes and communicate with the controlling machine through a JSON protocol over the standard output. When Ansible is not managing nodes, it does not consume resources because no daemons or programs are executing for Ansible in the background. Ansible uses one or more inventory files to manage the configuration of the multiple nodes in the system.

More information about Ansible can be found at [http://docs.ansible.com](http://docs.ansible.com)

