# Appendix A: Bill of Materials

The following BOM contains electronic license to use (E-LTU) parts. Electronic software license delivery is now available in most 
countries. HPE recommends purchasing electronic products over physical products (when available) for faster delivery and for the 
convenience of not tracking and managing confidential paper licenses. For more information, please contact your reseller or 
an HPE representative.

**Note:** Part numbers are at time of publication and subject to change. The bill of materials does not include complete 
support options or other rack and power requirements. If you have questions regarding ordering, please consult 
with your HPE Reseller or HPE Sales Representative for more details. 
[hpe.com/us/en/services/consulting.html](http://hpe.com/us/en/services/consulting.html).


**Table.** Bill of Materials

|Quantity|Part&#160;number|Description|
|:-------|:--------|:----------|
|1|Q8D81A|HPE SimpliVity 380 Gen10 Node|
|1|Q8D81A 001|HPE SimpliVity 380 Gen10 VMware Solution|
|1|826870-L21|HPE DL380 Gen10 Intel Xeon-Gold 6132 (2.6GHz/14-core/140W) FIO Processor Kit|
|1|826870-B21|HPE DL380 Gen10 Intel Xeon-Gold 6132 (2.6GHz/14-core/140W) Processor Kit|
|2|Q8D84A|HPE SimpliVity 192G 6 DIMM FIO Kit|
|1|Q5V86A|HPE SimpliVity 380 for 6000 Series Small Storage Kit|
|1|P01366-B21|HPE 96W Smart Storage Battery (up to 20 Devices) with 145mm Cable Kit|
|1|804331-B21|HPE Smart Array P408i-a SR Gen10 (8 Internal Lanes/2GB Cache) 12G SAS Modular Controller|
|1|700751-B21|HPE FlexFabric 10Gb 2-port 534FLR-SFP+ Adapter|
|1|BD505A|HPE iLO Advanced 1-server License with 3yr Support on iLO Licensed Features|
|1|Q8A60A|HPE OmniStack 2P Small SW|
|1|720865-B21|HPE 2U Cable Management Arm for Ball Bearing Rail Kit|
|1|867809-B21|HPE Gen10 2U Bezel Kit|
|1|826703-B21|HPE DL380 Gen10 SFF Systems Insight Display Kit|
|1|720863-B21|HPE 2U Small Form Factor Ball Bearing Rail Kit|
|1|H1K92A3|R2M HPE iLO Advanced Non Blade Support|



## Software Licenses

Licenses are required for the following software components:

-   VMware
-   Red Hat Enterprise Linux Server
-   Red Hat OpenShift Container Platform
