# Scale up your cluster with additional worker nodes

Once your OpenShift cluster has been successfully deployed, you can scale up the cluster with additional compute resources. These worker nodes are typically used for scheduling application workloads, so the number and size of the worker nodes will depend on the applications you wish to deploy in your OCP cluster.

With OCP 4.1, two types of worker nodes are supported:

- Red Hat Core OS
- Red Hat Enterprise Linux (RHEL) 7.6

