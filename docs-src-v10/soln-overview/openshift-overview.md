# Red Hat OpenShift Container Platform overview

Red Hat OpenShift Container Platform 4 is a consistent, managed Kubernetes experience which seamlessly connects workloads between on-premise datacenters and public cloud footprints. OpenShift 4 drives developer productivity while limiting operational complexities with built-in automation.


## Trusted enterprise Kubernetes

Red Hat OpenShift Container Platform is certified, conformant Kubernetes, as validated by the Cloud Native Computing
Foundation (CNCF). It is the only enterprise Kubernetes offering built on the backbone of the world’s leading enterprise
Linux platform backed by the open source expertise, compatible ecosystem, and leadership of Red Hat. As a top Kubernetes
community contributor, Red Hat refines Kubernetes for enterprises in Red Hat OpenShift 4, providing a codebase that is hardened
and more secure while retaining key innovations from upstream communities.


## Red Hat Enterprise Linux CoreOS
To provide a more flexible deployment footprint while still maintaining enhanced security and stability, Red Hat OpenShift 4
introduces Red Hat Enterprise Linux CoreOS, an OpenShift-specific embedded variant of Red Hat Enterprise Linux. Red Hat
Enterprise Linux CoreOS provides expanded choice for enterprises in deploying enterprise-grade Kubernetes, offering a
lightweight, fully immutable, container-optimized Linux OS distribution. In this variant, security features and stability
are still paramount, with automated updates managed by Kubernetes and enabled by OpenShift with the push of a button.
This helps to reduce maintenance and improve business productivity.

## Kubernetes operators
Kubernetes operators support the complete automation for an application, by handling requirements such as updates,
backups, recovery from failure and scaling (up and down) without the need for human intervention. Operators extend
Kubernetes functionality in a standardized fashion, improving resiliency while reducing the load on operations staff. In
particular, in multi-cloud or hybrid cloud scenarios, they can shield developers of cloud-native applications from
variations across deployment environments.

Red Hat OpenShift Container Platform makes extensive use of Kubernetes operators to encode domain knowledge to correctly scale,
upgrade, and reconfigure your cluster and workloads, while protecting against data loss or unavailability. 

- Operators are built into OpenShift, so Kubernetes and cluster services are always up to date.
- Embedded OperatorHub provides a discovery marketplace for independent software vendor (ISV) operators, validated to run on OpenShift.

## Full-stack automated operations
Once the cluster and applications are deployed, life-cycle management for these components, consoles
for operators and developers, and security throughout the entire life cycle become critical.
Red Hat OpenShift Container Platform offers automated installation, upgrades, and life-cycle management for every
part of your container stack including the CoreOS operating system, Kubernetes, and cluster services and applications.
The result is a more secure, always-up-to-date Kubernetes application platform, without the headaches
of manual and serial upgrades, or downtime.

## Developer productivity

Developers can quickly and easily create applications on demand from the tools they use most, while Operations retains
full control over the entire environment. Integrated CI/CD pipelines let developers reduce manual deployment work and
deploy higher quality software for continuous integration and automated tests. Support for leading-edge tools such as
OpenShift Service Mesh and Knative serverless help drive developer productivity and innovation.


An introduction to the Red Hat OpenShift Container Platform architecture is available at
[https://docs.openshift.com/container-platform/4.1/architecture/architecture.html](https://docs.openshift.com/container-platform/4.1/architecture/architecture.html).

